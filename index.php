<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bulma
 */
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main wrapper" role="main">

		<section>
			<div class="container">

				<div class="tile is-ancestor">

				  <div class="tile is-3 is-parent">
  				    <div class="tile is-child box">
  				      <p class="title">One</p>
  				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
  				    </div>
  				  </div>
				  <div class="tile is-3 is-parent">
					<?php
						$term   = get_term( 2, 'tipo_etiqueta' );
						$bajada = get_field('bajada', 'tipo_etiqueta_2');
						$cta    = get_field('call_to_action', 'tipo_etiqueta_2');
						$img    = get_field('featured_image', 'tipo_etiqueta_2');
						$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_2');
						$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_2');
						$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_2');
						$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_2');
						$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_2');
						$deg_rot= get_field('rotacion_degrade', 'tipo_etiqueta_2');
					?>
  				    <div class="tile is-child box" style="color:#fff; background-image: linear-gradient(to right bottom, <?php echo $deg_c1.','.$deg_c2 ?>);">
  				      <p class="title"><?php echo $term->name ?></p>
					  <p class="bajada"><?php echo $bajada ?></p>
					  <p class="desc"><?php echo $term->description ?></p>
					  <p class="cta"><?php echo $cta ?></p>
					  <?php echo $img ?>

  				    </div>
  				  </div>
				  <div class="tile is-vertical is-parent">
				    <div class="tile is-child box">
				      <p class="title">Three</p>
					  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
				    </div>
				    <div class="tile is-child box">
				      <p class="title">Five</p>
					  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
				    </div>
				  </div>
				  <div class="tile is-vertical is-parent">
				    <div class="tile is-child box">
				      <p class="title">Four</p>
					  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
				    </div>
				    <div class="tile is-child box">
				      <p class="title">Six</p>
					  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
				    </div>
				  </div>

				</div>

			</div>
		</section>
		<?php if ( have_posts() ) : ?>
			<div class="container">
				<div class="columns is-multiline">
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="column is-one-third">
							<?php get_template_part( 'template-parts/content', 'post' ); ?>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
			<div class="section pagination">
				<div class="container">
					<?php the_posts_pagination(); ?>
				</div>
			</div>
		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
