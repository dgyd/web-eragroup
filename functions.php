<?php
/**
 * Bulmapress functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bulmapress
 */


require get_template_directory() . '/functions/bulmapress_navwalker.php';
require get_template_directory() . '/functions/bulmapress_helpers.php';
require get_template_directory() . '/functions/bulmapress_custom_query.php';

if ( ! function_exists( 'bulmapress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bulmapress_setup() {
	require get_template_directory() . '/functions/base.php';
	require get_template_directory() . '/functions/post-thumbnails.php';
	require get_template_directory() . '/functions/navigation.php';
	require get_template_directory() . '/functions/content.php';
	require get_template_directory() . '/functions/pagination.php';
	require get_template_directory() . '/functions/widgets.php';
	require get_template_directory() . '/functions/search.php';
	require get_template_directory() . '/functions/scripts-styles.php';
}
endif;
add_action( 'after_setup_theme', 'bulmapress_setup' );

require get_template_directory() . '/functions/template-tags.php';
require get_template_directory() . '/functions/extras.php';
require get_template_directory() . '/functions/customizer.php';
require get_template_directory() . '/functions/jetpack.php';
require get_template_directory() . '/functions/shortcodes.php';

// Register the column
function department_add_dynamic_hooks() {
	$taxonomy = 'tipo_etiqueta';
	add_filter( 'manage_' . $taxonomy . '_custom_column', 'department_taxonomy_rows',15, 3 );
	add_filter( 'manage_edit-' . $taxonomy . '_columns',  'department_taxonomy_columns' );
	}
	add_action( 'admin_init', 'department_add_dynamic_hooks' );

	function department_taxonomy_columns( $original_columns ) {
	$new_columns = $original_columns;
	array_splice( $new_columns, 1 );
	$new_columns['frontpage'] = esc_html__( 'Tipo de Etiqueta', 'taxonomy-images' );
	return array_merge( $new_columns, $original_columns );
	}

	function department_taxonomy_rows( $row, $column_name, $term_id ) {
	$t_id = $term_id;
	$meta = get_option( "taxonomy_$t_id" );
	if ( 'frontpage' === $column_name ) {
		if ($meta == true) {
			return $row . 'Yes';
		} else {
			return $row . 'No';
		}
	}
}


// Add the custom columns to the etiqueta post type: See more here https://wordpress.stackexchange.com/questions/253640/adding-custom-columns-to-custom-post-types
add_filter( 'manage_etiqueta_posts_columns', 'set_custom_edit_etiqueta_columns' );
function set_custom_edit_etiqueta_columns($columns) {
    unset( $columns['author'] );
    $columns['tipo_etiqueta'] = __( 'Tipo de Etiqueta', 'your_text_domain' );

    return $columns;
}

// Add the data to the custom columns for the etiqueta post type:
add_action( 'manage_etiqueta_posts_custom_column' , 'custom_etiqueta_column', 10, 2 );
function custom_etiqueta_column( $column, $post_id ) {
    switch ( $column ) {

        case 'tipo_etiqueta' :
            $terms = get_the_term_list( $post_id , 'tipo_etiqueta' , '' , ', ' , '' );
            if ( is_string( $terms ) )
                echo $terms;
            else
                _e( 'Sin asignar', 'your_text_domain' );
            break;

    }
}



function my_admin_scripts() {
	wp_deregister_script( 'jquery' );
  }
add_action( 'wp_enqueue_scripts', 'my_admin_scripts' );



/**
 * Change display to fullscreen in SuperPWA manifest
 *
 * @param (array) $manifest An array containing all manifest parameters.
 *
 * @author Arun Basil Lal
 */
function kilinkis_set_display_as_fullscreen_superpwa( $manifest ) {

	$manifest['display'] = 'fullscreen';

	return $manifest;
}
add_filter( 'superpwa_manifest', 'kilinkis_set_display_as_fullscreen_superpwa' );
