/* eslint-disable */
(function() {
  //console.log("elems");
  var elems = document.querySelectorAll(
    ".wpcf7-list-item input[type='checkbox']"
  );
  //console.log(elems);
  for (var i = 0; i < elems.length; i++) {
    var switchery = new Switchery(elems[i]);
  }

  $('a.ask').on('click', function(e) {
    e.preventDefault();
    var contactForm = document.getElementById('contacto');
    contactForm.scrollIntoView();
    // fill in contact
    contactForm.querySelector('.wpcf7-textarea').value =
      'Estoy interesado en etiquetas de tipo ' + $(this).prev().find('figcaption').text().replace(/\s\s+/g, ' ');
  });

})();

function closeModalposts() {
  const modal = document.querySelector('.modal');
  modal.classList.remove('is-active');
  // goto anchor
  document.getElementById('contacto').scrollIntoView();
}
