<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="is-fullheight">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700|Open+Sans:400,700" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- <script src="https://unpkg.com/core-js/client/shim.min.js"></script> -->

	<?php // load this two with wp_register_script ?>
	<script src="<?php echo get_template_directory_uri(); ?>/frontend/js/shuffle.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/frontend/js/tippy.all.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/frontend/js/basicLightbox.min.js"></script>
	<link href="<?php echo get_template_directory_uri(); ?>/frontend/css/basicLightbox.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-51603018-1', 'eragroup.com.ar');
	  ga('send', 'pageview');

	</script>
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '990005294402095');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=990005294402095&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<?php wp_head(); 

	$banner = get_field('banner');

	if(!$banner){ ?>

	<script>
	// read url campaign
	function getUrlVar(key){
		var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
		return result && unescape(result[1]) || "";
	}
	const campaign = getUrlVar("utm_campaign");
	const filter = getUrlVar("filter");
	let selector = "banner-"+campaign;
	//element.classList.add("mystyle");
	//jQuery( document ).ready(function() {
	document.addEventListener("DOMContentLoaded",function(){
		if( selector != 'banner-' ) {
			//jQuery('#'+selector).removeClass('is-hidden');
			const el = document.getElementById(selector);
			el.classList.remove("is-hidden");
		}
	});
	</script>

<?php  } 	?>

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-933853391"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-933853391'); </script>

<script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-933853391/e9PTCKrvsqUBEM_xpb0D', 'event_callback': callback }); return false; } </script>

<script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:1397285,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</head>

<body <?php body_class(); ?>>

	<div id="myModal" class="modal">
		<div class="modal-background"></div>
		<div class="modal-content">

			<div class="share-contents">
				
				<div class="share-content"></div>
				
				<div class='social-share-btns-container'>
					<div class='social-share-btns'>
						<a class='share-btn share-btn-twitter' href='' data-sharer="https://twitter.com/intent/tweet?text=" rel='nofollow' target='_blank'>
							<img src='https://icon.now.sh/twitter/ffffff' alt='' />
							Tweet
						</a>
						<a class='share-btn share-btn-facebook' href='' data-sharer="https://www.facebook.com/sharer/sharer.php?u=" rel='nofollow' target='_blank'>
							<img src='https://icon.now.sh/facebook/ffffff' alt='' />
							Compartir
						</a>
						<a class='share-btn share-btn-linkedin' href='' data-sharer="https://www.linkedin.com/cws/share?url=" rel='nofollow' target='_blank'>
							<img src='https://icon.now.sh/linkedin/ffffff' alt='' />
							Compartir
						</a>
						<a class='share-btn share-btn-reddit' href='' data-sharer="https://api.whatsapp.com/send?phone=&text=Visit%c3%a1%20este%20enlace%20para%20ver%20m%c3%a1s%20referencias%20como%20esta%2c%20o%20compartilo%20con%20uno%20de%20nuestros%20asesores%20para%20cotizar%20tu%20pr%c3%b3xima%20etiqueta.%20%0d%0a%0d%0ainfo%40eragroup.com.ar%20%7c%20%28011%29%205254-1823%20%2f%201732%20%0d%0aWhatsapp%20MDQ%20223%205%20202%20102%20%7c%20Whatsapp%20BA%2011%202683%209337%20%0d%0a%0d%0awww.eragroup.com.ar%20" rel='nofollow' target='_blank'>
							<img src='https://icon.now.sh/whatsapp/ffffff' alt='' />
							Compartir
						</a>
						<a class='share-btn share-btn-mail' href='' data-sharer="mailto:?subject=Etiquetas Era Group&amp;amp;body=Visitá%20este%20enlace%20para%20ver%20más%20referencias%20como%20esta,%20o%20compartilo%20con%20uno%20de%20nuestros%20asesores%20para%20cotizar%20tu%20próxima%20etiqueta.%20%0D%0A%0D%0A

	info@eragroup.com.ar%20|%20(011)%205254-1823%20/%201732%20%0D%0A
	Whatsapp%20MDQ%20223%205%20202%20102%20|%20Whatsapp%20BA%2011%202683%209337%20%0D%0A%0D%0A

	www.eragroup.com.ar%20" rel='nofollow' target='_blank' title='via email'>
							<img src='https://icon.now.sh/email/ffffff' alt='' />
							Compartir
						</a>
					</div>
				</div>

			</div>

			<div class="post-contents">
			</div>

			<!-- <span class="close-modal">&times;</span> -->
			<button class="modal-close is-large" aria-label="close"></button>

		</div>
	</div>

	<div id="page" class="site">
		<?php bulmapress_skip_link_screen_reader_text(); ?>

		<header id="header">

			<nav id="site-navigation" class="navbar" role="navigation">
				<div class="container">

					<div class="navbar-brand">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				       <img src="<?php echo get_template_directory_uri(); ?>/frontend/img/eragroup_logo.jpg" class="logo" alt="EraGroup logo" />
				    </a>
						<?php bulmapress_menu_toggle(); ?>
					</div>

					<div class="navbar-menu">
						<div class="navbar-start">
							<a class="navbar-item" href="<?php echo esc_url( home_url( '/' ) ); ?>catalogo">
							  <img class="catalogo" src="<?php echo get_template_directory_uri(); ?>/frontend/img/open-book.svg" /> Catálogo
							</a>
						</div>

						<div class="navbar-end">
							<!-- <a class="navbar-item" href="/sample">
							  Acerca
							</a> -->
							<a class="navbar-item" href="/preguntas-frecuentes">
							  FAQs
							</a>
							<a class="navbar-item has-border" href="/#contacto">
							  Contactarse
							</a>
						</div>

						<?php //bulmapress_navigation(); ?>
					</div>

				</div>
			</nav>

		</header>

		<div id="content" class="site-content">


			<?php // move to template_part function ?>
			<div id="banner-bordadas" class="banner-bordadas <?php echo ($banner == 'bordadas') ? '' : 'is-hidden' ?>">
				<div class="container columns">
					<div class="column has-text-centered">
						<div class="has-text-left">
							<small>Aterrizaste justo en lo que querias</small>
						</div>
						<div>
							<p>Etiquetas bordadas</p>
						</div>
						<div class="has-text-right">
							<small><a href="#">Ver catálogo de etiquetas bordadas &rarr;</a></small>
						</div>
					</div>
					<div class="column has-text-right">
						<a href="#contacto" class="button is-primary">Consultar ahora</a>
					</div>
				</div>
			</div>

			<div id="banner-estampadas" class="banner-bordadas <?php echo ($banner == 'estampadas') ? '' : 'is-hidden' ?>">
				<div class="container columns">
					<div class="column has-text-centered">
						<div class="has-text-left">
							<small>Aterrizaste justo en lo que querias</small>
						</div>
						<div>
							<p>Etiquetas Estampadas</p>
						</div>
						<div class="has-text-right">
							<small><a href="#">Ver catálogo de etiquetas bordadas &rarr;</a></small>
						</div>
					</div>
					<div class="column has-text-right">
						<a href="#contacto" class="button is-primary">Consultar ahora</a>
					</div>
				</div>
			</div>
