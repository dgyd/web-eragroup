module.exports = {
	env: {
		browser: true,
		es6: true,
	},
	extends:
		"airbnb",
	parserOptions: {
		ecmaVersion: 2016,
	},
	rules: {
		indent: [
			"error",
			"tab",
		],
		"no-console": 0,
		quotes: [
			"error",
			"double",
		],
		semi: [
			"error",
			"never",
		],
		"no-unused-vars": 1,
	},
	plugins: [
		"html",
	],
}
