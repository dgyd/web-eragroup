<?php

/*
*
*       IF MORE SHORTCODES ARE ADDED, BETTER DO IT IN A SEPARATE FILE AND THEN IMPORT IT HERE
*
*/


// catalogue shortcode
function catalogue_wp_enqueue_scripts() {
	wp_register_script( 'filters', get_template_directory_uri().'/frontend/js/filters.js' , '', '', true );
}

add_action( 'wp_enqueue_scripts', 'catalogue_wp_enqueue_scripts' );
function show_catalog( $atts , $content = null ) {

	wp_enqueue_script( 'filters' );

	// Attributes
	$atts = shortcode_atts(
		array(
			'tipo' => '',
			'title' => '',
			'text_filter' => 'yes',
			'button_filter' => 'yes',
			'order_filter' => 'yes',
			'active' => '',
			'popups' => '',
			'quantity' => -1,
			'random' => false
		),
		$atts
	);

	ob_start(); ?>




	<div class="container wrapper-catalogo">

		<div class="columns">

			<?php if( $atts['text_filter'] == 'yes' ) { ?>
			<div class="column is-one-third">
				<div class="filters-group field">
				<label for="filters-search-input label" class="filter-label">Filtro de texto</label>
				<div class="control">
					<input class="textfield filter__search js-shuffle-search input" type="search" id="filters-search-input">
				</div>
				</div>
			</div>
			<?php } ?>

			<div class="column columns filters-group-wrap" id="filters">
				<?php if( $atts['button_filter'] == 'yes' ) { ?>
				<div class="filters-group column is-one-half is-hidden-mobile">
					<?php // <p class="filter-label">Filtros</p> ?>
					<div class="btn-group filter-options">
						<button class="button is-primary-era" data-group="etiquetas-bordadas"><span>Bordadas</span></button>
						<button class="button is-primary-era" data-group="etiquetas-goma"><span>Goma</span></button>
						<button class="button is-primary-era" data-group="etiquetas-cuero"><span>Cuero</span></button>
						<button class="button is-primary-era" data-group="etiquetas-estampadas"><span>Estampadas</span></button>
						<button class="button is-primary-era" data-group="etiquetas-hangtag"><span>Colgante</span></button>
					</div>
				</div>
				<?php }

				if( $atts['order_filter'] == 'yes' ) { ?>
				<fieldset class="filters-group column is-one-half">
					<legend class="filter-label">Orden</legend>
					<div class="btn-group sort-options">
						<label class="btn active">
							<input type="radio" name="sort-value" value="dom"> Default
						</label>
						<label class="btn">
							<input type="radio" name="sort-value" value="title"> Alfabetico
						</label>
						<label class="btn">
							<input type="radio" name="sort-value" value="date-created"> Fecha
						</label>
					</div>
				</fieldset>
				<?php } ?>
			</div>

		</div>

		<p class="section-title pt10 pb10"><?php echo $atts['title'] ?></p>
		<div id="" class="columns grid-filter">


<?php

$orderby = 'default';
if($atts['random']){
	$orderby = 'rand';
}

$args = array( 
	
	'post_type' => 'etiqueta',
	'posts_per_page' => $atts['quantity'],
	'tipo_etiqueta' => $atts['tipo'],
	'orderby'   => $orderby

);

$loop = new WP_Query( $args );
global $post;

while ( $loop->have_posts() ) : $loop->the_post();

	$tipos_etiqueta = wp_get_object_terms($post->ID, 'tipo_etiqueta');

	$data_group = '[';
	$data_group_names = '';
	foreach ($tipos_etiqueta as $tipo_etiqueta) {
		if($data_group == '['){ // si es el primer term, no agrego comma
			$comma = '';
		} else {
			$comma = ',';
		}
		$data_group       .= $comma.'&quot;'.$tipo_etiqueta->slug.'&quot;';
		$data_group_names .= $comma.' '.$tipo_etiqueta->name;
	}
	$data_group .= ']';

	$current_title = get_the_title();

	?>

				<figure class="column is-one-quarter picture-item shuffle-item shuffle-item--visible br6"
						data-groups="<?php echo $data_group ?>"
						data-date-created="<?php echo get_the_date( 'Y-m-d' )?>"
						data-title="<?php echo $current_title ?>"
						title="<?php echo $current_title ?>"
						>
					<div class="picture-item__inner">

						<div class="aspect aspect--16x9">
							<div class="aspect__inner">

								<?php
								if ( get_the_post_thumbnail_url($post->ID, 'thumb') != "" ) { ?>

										<!-- <img class="thumbnail"
											src="<?php echo get_the_post_thumbnail_url($post->ID, 'thumb'); ?>"
											data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>"
											data-src-full="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" /> -->
										<?php echo apply_filters( 'jetpack_photon_url', get_the_post_thumbnail(get_the_ID(), 'full', array( 'class' => 'nolazy' ))); ?>
										<?php //the_post_thumbnail('thumb'); ?>
										<!-- <img src="http://wocker.test/wp-content/uploads/2014/04/B_SATEN_1-1-1024x732.jpg"> -->


								<?php }
								else {
									echo '<img src="' . get_bloginfo( 'stylesheet_directory' )
										. '/frontend/img/thumbnail-default.jpg" />';
								} ?>

							</div>
						</div>

						<div class="picture-item__details">
								<div class="description-wrapper">
									<figcaption class="picture-item__title has-text-weight-bold">
										<a href="#" target="_blank" rel="noopener"><?php echo $current_title ?></a>
									</figcaption>
									<p class="picture-item__tags has-text-weight-light"><?php echo $data_group_names; ?></p>
								</div>
								<a href="#" class="ask"><i class="fa fa-question-circle" aria-hidden="true"></i></a>
						</div>

						<?php // <p class="picture-item__description">SpaceX launches a Falcon 9 rocket from Cape Canaveral Air Force Station</p> ?>

					</div>
					<?php if( $atts['popups'] == 'yes' ){ ?>
					<div class="popup">
						<?php if($data_group_names) { ?>
						<h6>Tipo de etiqueta</h6>
						<p><?php echo $data_group_names; ?></p>
						<?php }
							if(get_field('design')) {
						?>
						<h6>Diseño</h6>
						<p><?php echo get_field('design') ?> </p>
						<?php }
							if(get_field('medida')) {
						?>
						<!-- <h6>Medida</h6>
						<p><?php echo get_field('medida') ?> </p> -->
						<?php }
						if(get_field('design')) {
							?>
							<h6>Calidad</h6>
							<p><?php echo get_field('calidad') ?> </p>
							<?php }
							if(get_field('terminacion')) {
						?>
						<h6>Terminación</h6>
						<p><?php echo get_field('terminacion') ?> </p>
						<?php } ?>
						<div class="is-flex buttons-wrapper">
							<a href="#" id="brnPrepend" class="era-sharable-link" data-etiqueta-link="<?php echo the_permalink(); ?>">
								<button class="button colored">Compartir</button>
							</a>
							<a id="brnConsulta" data-etiqueta-tipo="<?php echo $data_group_names; ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>contacto/?producto=<?php echo $current_title ?>">
								<button class="button">Consultar</button>
							</a>
						</div>

					</div>
					<?php } ?>
				</figure>

<?php
endwhile;
wp_reset_postdata();

?>

	</div>

</div>
				<!-- <div class="sizer column is-one-quarter"></div> -->
<script>
document.addEventListener("DOMContentLoaded", () => {
	const cols = document.querySelectorAll('.grid-filter');

	[].forEach.call(cols, (e)=>{
		window.demo = new Demo(e)
	});
	//window.demo = new Demo(document.querySelector(".grid-filter"))
	//window.demo = new Demo(document.querySelector(".catalogo-grid-2 .grid-filter"))
})

// document.addEventListener('click',function(e) {
// 	if(e.target && e.target.id== 'brnPrepend'){
// 		e.preventDefault();
// 		alert('click');
// 	}

// });

</script>





<?php

return ob_get_clean();

}
add_shortcode( 'catalogo', 'show_catalog' );