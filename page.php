<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */

$subtitle = get_field('subtitulo');
$header = get_field('header_extendido');
$sidebar = get_field('mostrar_sidebar');
$icon = get_field('icono');

if ($sidebar == 'sidebar') {
	$sidebar = true;
} else {
	$sidebar = false;
}

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>

		<article <?php post_class('section is-paddingless'); ?>>

			<header class="entry-header <?php echo $header ?>">
				<div class="container content is-flex">
					<?php
					if ($header=='extendido') { echo '<p class="subtitle">'.$subtitle.'</p>'; }
					the_title( '<h1 class="title is-1 entry-title">', '</h1>' );
					?>
					<div class="header-icon">
						<!-- <i class="fa fa-facebook" aria-hidden="true"></i> -->
						<img class="catalogo" src="<?php echo $icon ?>" />
					</div>
				</div>
			</header><!-- .entry-header -->

			<div class="container columns content is-desktop">
				<section class="content entry-content column is-12 <?php if ($sidebar) { echo "is-8-desktop"; } ?>">
					<?php the_content();?>
				</section><!-- .entry-content -->
				<?php if ($sidebar) {
					echo '<div class="column is-12 is-4-desktop">';
					get_sidebar();
					echo '</div>';
				} ?>
			</div>
		</article><!-- #post-## -->

		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/contact', 'row' ); ?>
		
	</main><!-- #main -->
</div><!-- #primary -->
<script>
    tippy('.picture-item', {
    html: el => el.querySelector('.popup'),
    interactive: true,
	flip: false
    });

    document.querySelectorAll('.thumbnail').forEach(function(elem) {

        elem.onclick = function(e) {

            const src = elem.getAttribute('data-src')
			const src_full = elem.getAttribute('data-src-full')
            const html = '<a href="'+src_full+'" target="_blank"><img src="' + src + '" class="lightbox-image" /></a>'

            basicLightbox.create(html).show()

            document.querySelector(".basicLightbox__placeholder").onclick = function() {closeLightbox()};
            function closeLightbox() {
                document.querySelector('.basicLightbox').remove();
            }

        }

    })
</script>
<?php
get_footer(); ?>
