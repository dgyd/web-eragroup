<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer hero" role="contentinfo">
	<div class="container">

		<div class="columns">
			<div class="column">
				<h3>EraGroup Etiquetas</h3>
				<ul class="footer-widget">
					<li><a href="/##">Nuestra Empresa</a></li>
					<li><a href="/catalogo">Catálogo de Etiquetas</a></li>
					<li><a href="/catalogo">Catálogo de Colores</a></li>
					<li><a href="/catalogo">Técnicas y Materiales</a></li>
					<li><a href="/catalogo">Protección de Marcas</a></li>
					<li><a href="#contacto">Contacto</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Productos</h3>
				<ul class="footer-widget">
					<li><a href="/catalogo">Etiquetas Bordadas</a></li>
					<li><a href="/catalogo">Etiquetas Estampadas</a></li>
					<li><a href="/catalogo">Etiquetas de Cuero</a></li>
					<li><a href="/catalogo">Etiquetas de Plastisol</a></li>
					<li><a href="/catalogo">Etiquetas Hangtag</a></li>
					<li><a href="/catalogo">Bolsas Impresas</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Contacto</h3>
				<ul class="footer-widget">
					<li itemprop="streetAddress"><a href="https://goo.gl/maps/mEpMfbdy5xumXTzF6">Balcarse 923, San Telmo</a></li>
					<li itemprop="Locality"><a href="https://goo.gl/maps/mEpMfbdy5xumXTzF6">San Telmo, CABA, Argentina</a></li>
					<li itemprop="email"><a href="mailto:info@eragroup.com">info@eragroup.com</a></li>
					<li><a href="https://web.whatsapp.com/send?phone=5491132519188&text=Hola!,%20necesitar%C3%ADa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20etiquetas.">Whatsapp 11 268 393 37</a></li>
					<li itemprop="telephone"><a href="tel:+541152541823">011 5254-1823</a></li>
					<li itemprop="telephone"><a href="tel:+541152541732">011 5254-1732</a></li>
				</ul>
			</div>
			<div class="column">
				<h3>Nosotros te llamamos</h3>
				<?php echo do_shortcode('[contact-form-7 id="558" title="footer"]'); ?>
			</div>
		</div>


		<div class="columns copyright">
			<div class="column is-three-quarters">
				<ul>
					<li><a href="/catalogo">Bordadas</a></li>
					<li><a href="/catalogo">Estampadas</a></li>
					<li><a href="/catalogo">Plásticas</a></li>
					<li><a href="/catalogo">Bandanas</a></li>
					<li><a href="/catalogo">Hangtags</a></li>
					<li><a href="/catalogo">Bolsas</a></li>
				</ul>
				<p>&reg; Todo el contenido en este sitio está protegido por las leyes de patentes. Prohibida su reproducción sin consentimiento.</p>
			</div>
			<div class="column">
				<ul class="social-icons">
					<li><a href="https://www.instagram.com/eragroupetiquetas/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="https://www.facebook.com/eragroupetiquetas/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>

	</div><!-- .container -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript" src="https://abouolia.github.io/sticky-sidebar/js/rAF.js"></script>
<script type="text/javascript" src="https://abouolia.github.io/sticky-sidebar/js/ResizeSensor.js"></script>
<script type="text/javascript" src="https://abouolia.github.io/sticky-sidebar/js/sticky-sidebar.js"></script>
<script type="text/javascript">

	var a = new StickySidebar('#filters', {
		//topSpacing: 20,
		//bottomSpacing: 20,
		containerSelector: '.wrapper-catalogo',
		//innerWrapperSelector: '.sidebar__inner'
	});
</script>

</body>
</html>
