<?php
/**
 * Template name: Catalogo
 */

$subtitle = get_field('subtitulo');
$header = get_field('header_extendido');
$sidebar = get_field('mostrar_sidebar');
$icon = get_field('icono');

if ($sidebar == 'sidebar') {
	$sidebar = true;
} else {
	$sidebar = false;
}

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>

		<article <?php post_class('section is-paddingless'); ?>>

			<header class="entry-header <?php echo $header ?>">
				<div class="container content is-flex">
					<?php
					if ($header=='extendido') { echo '<p class="subtitle">'.$subtitle.'</p>'; }
					the_title( '<h1 class="title is-1 entry-title">', '</h1>' );
					?>
          <script>
            // if( filter != ''){
            //   var node = document.createElement("span");                 // Create a <li> node
            //   var textnode = document.createTextNode(" - "+filter);         // Create a text node
            //   node.appendChild(textnode);        
            //   document.querySelector('.entry-title').appendChild(node);
            // }
          </script>
					<div class="header-icon">
						<!-- <i class="fa fa-facebook" aria-hidden="true"></i> -->
						<img class="catalogo" src="<?php echo $icon ?>" />
					</div>
				</div>
			</header><!-- .entry-header -->

			<div class="container columns content is-desktop">
				<section class="content entry-content column is-12 <?php if ($sidebar) { echo "is-8-desktop"; } ?>">
					<?php the_content();?>
				</section><!-- .entry-content -->
				<?php if ($sidebar) {
					echo '<div class="column is-12 is-4-desktop">';
					get_sidebar();
					echo '</div>';
				} ?>
			</div>
		</article><!-- #post-## -->

		<?php endwhile; ?>

		<?php get_template_part( 'template-parts/contact', 'row' ); ?>
		
	</main><!-- #main -->
</div><!-- #primary -->
<script>
    tippy('.picture-item', {
    html: el => el.querySelector('.popup'),
    interactive: true,
		flip: false,
		onShow: function() {
			this.querySelector('#brnPrepend').addEventListener("click", function(e){
				var sharableURL = this.getAttribute('data-etiqueta-link');
				
				// Get the modal
				var modal = document.getElementById('myModal');
				var modalBG = document.getElementsByClassName("modal-background")[0];
				var span = document.getElementsByClassName("modal-close")[0];
				var shareContent = document.getElementsByClassName("share-content")[0];
				
				modal.classList.add("is-active");
					
				span.onclick = function() {
					modal.classList.remove("is-active");
				}
				
				// When the user clicks anywhere outside of the modal, close it
				window.onclick = function(event) {
					if (event.target == modalBG) {
						modal.classList.remove("is-active");
					}
				}
				
				// edit modal content
				//shareContent.innerHTML = sharableURL;
				
				// for each que agarra todos los botones del popup, copie lo tengan en el href, y agregen sharableURL al final
				document.querySelectorAll('.is-active a.share-btn').forEach(function(elem, i) {
					//if(elem.href.indexOf("sharableURL") == -1 ) {
						elem.href = elem.getAttribute('data-sharer') + sharableURL;
					//}
				});
				
				e.preventDefault();
			});
			this.querySelector('#brnConsulta').addEventListener("click", function(e){
				e.preventDefault();
				var contactForm = document.getElementById('contacto');
				contactForm.scrollIntoView();
				// fill in contact
				contactForm.querySelector('.wpcf7-textarea').value = "Estoy interesado en etiquetas de tipo" + this.getAttribute('data-etiqueta-tipo');
			});
		}
    });

    document.querySelectorAll('.thumbnail').forEach(function(elem) {

        elem.onclick = function(e) {

            const src = elem.getAttribute('data-src')
			const src_full = elem.getAttribute('data-src-full')
            const html = '<a href="'+src_full+'" target="_blank"><img src="' + src + '" class="lightbox-image" /></a>'

            basicLightbox.create(html).show()

            document.querySelector(".basicLightbox__placeholder").onclick = function() {closeLightbox()};
            function closeLightbox() {
                document.querySelector('.basicLightbox').remove();
            }

        }

    })
</script>
<?php
get_footer(); ?>
