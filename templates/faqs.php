<?php
/**
 * Template name: Faqs
 *
 */

$subtitle = get_field('subtitulo');
$header = get_field('header_extendido');
$sidebar = get_field('mostrar_sidebar');
$icon = get_field('icono');

if ($sidebar == 'sidebar') {
	$sidebar = true;
} else {
	$sidebar = false;
}

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>

		<article <?php post_class('section is-paddingless'); ?>>

			<header class="entry-header <?php echo $header ?>">
				<div class="container content is-flex">
					<?php
					if ($header=='extendido') { echo '<p class="subtitle">'.$subtitle.'</p>'; }
					the_title( '<h1 class="title is-1 entry-title">', '</h1>' );
					?>
					<div class="header-icon">
						<!-- <i class="fa fa-facebook" aria-hidden="true"></i> -->
						<img class="catalogo" src="<?php echo $icon ?>" />
					</div>
				</div>
			</header><!-- .entry-header -->

			<div class="container columns content is-desktop">
				<section class="content entry-content column is-12 <?php if ($sidebar) { echo "is-8-desktop"; } ?>">
          <?php the_content();
          
					// devlands!!!
						$args = array(
							'post_type' => 'faq',
							'post_status' => 'publish',
							'orderby' => 'menu_order',
							'order' => 'ASC',
							'posts_per_page' => 9999
						);

						$gp_query = new WP_Query($args);
									
						if ($gp_query->have_posts()) { ?>
						
							<?php 
							$cont = 1;
							while ($gp_query->have_posts()) { 
								$gp_query->the_post();
								global $post;
								?>
								<!-- post entry -->
								<article class="post single clearfix">
									<div class="post-content">
										<a href="#<?php echo $post->post_name; ?>">
                      <h3><strong><span style="color: #46b05d;"><?php the_title() ?><br> </span></strong></h3>
                    </a>
									</div>
								</article>
								<!-- end post entry -->
								
				
							<?php }	
						}
					?>
					<hr>
					<?php 
					if ($gp_query->have_posts()) { ?>
						
							<?php 
							$cont = 1;
							while ($gp_query->have_posts()) { 
								$gp_query->the_post(); 
								global $post;
								?>
								<!-- post entry -->
								<article class="post single clearfix">
									<div class="post-content">
										<h3 id="<?php echo $post->post_name  ?>"><strong><span style="color: #46b05d;"><?php the_title() ?><br> </span></strong></h3>
										<p><?php the_content(); ?></p>
									</div>
								</article>
								<!-- end post entry -->
								
				
							<?php }	
						}
					?>


				</section><!-- .entry-content -->
				<?php if ($sidebar) {
					echo '<div class="column is-12 is-4-desktop">';
					get_sidebar();
					echo '</div>';
				} ?>
			</div>
		</article><!-- #post-## -->

		<?php endwhile; ?>
		<?php get_template_part( 'template-parts/contact', 'row' ); ?>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer(); ?>
