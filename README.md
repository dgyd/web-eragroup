# EraGroup web 2.0

run this: https://cssstats.com/

To run locally:

npm install and then:

yarn run start

to install packages: (hint: js is not bundled, so use cdn or add it to the theme)

yarn install --dev package

Syncing DB

use https://ngrok.com/

Download and then start ngrok

cd Downloads
./ngrok http wocker.test:80

then, copy the provided and replace it in wp_options (local)

now you can access wocker.test through the tunnel with the url provided by ngrok

access the remote website and make sure the sync-db plugin is active on both sites

go to Settings on the local installation, copy the credentials
go to the Import tab on the remote website, and choose PULL
paste the credentials and you should be good to go

load more etiquetas:
https://vestride.github.io/Shuffle/ajax

tecnology stack:

docker
wordpress
mariadb
git
bitbucket
WP
underscores
bulma
sass
webpack
ES6
CI/CD (bitbucket pipelines only on master)

TODO:
make widgets and menus dynamic

Email migration:
Originalmente, teniamos en el servidor dedicado, una pagina web y un sistema de administracion.
La semana pasada movimos mi web a otro host (no es donweb), dejando en el servidor dedicado solamente el sistema de administracion.

para mantener no tener que migrar los emails, solo cambie el registro A de los DNS, apuntandolo al nuevo servidor. Este cambio lo hice desde la Zona de DNS en el Ferozo, seleccionando el dominio eragroup.com.ar

https://intodns.com/eragroup.com.ar
https://www.dnswatch.info/dns/dnslookup?la=en&host=eragroup.com.ar&type=MX&submit=Resolve

long story short:
como donweb no permite mx recods con valores que no tengan el dominio en el nombre (mx.eragroup.com.ar), y el dominio esta apuntado a otro server, los emails dejaron de andar. La solucion fue hacer un registro A en el nuevo host, que redireccione a la IP de donweb.

Hay tickets en don web y en siteground.

eslint

node_modules/.bin/eslint js/filters.js

old website: https://www.eragroup.com.ar/old

Also, we are using Branchci now
By Peter
