<section id="contacto" class="contact">
    <div class="container is-fluid">
        <div class="columns contact-toggle is-flex m0 p0 is-marginless">
            <div class="column pt20">
                <p>CONTACTATE CON NOSOTROS</p>
            </div>
            <div class="column pt20 has-text-right">
                <!-- <p class="has-text-weight-light">Ocultar formulario <i class="fa fa-chevron-circle-up" aria-hidden="true"></i></p> -->
            </div>
        </div>
    </div>
    
    <div id="contact-wrap" class="show">
        <div class="columns max-100 is-marginless">
            <div class="column col1">
            <div class="direction-row pb20">
                <img width="100" src="<?php echo get_template_directory_uri(); ?>/frontend/img/iconos_eragroup.png" class="icons" alt="EraGroup icons" />
            </div>
            <p class="big-text has-text-weight-semibold">Etiquetas  a la  medida  de  tus  proyectos</p>
            <div class="direction-row is-flex flex-align-center flex-end pb20">
                <img width="200" src="<?php echo get_template_directory_uri(); ?>/frontend/img/era-logo-invert.png" class="icons" alt="EraGroup icons" />
            </div>
        </div>
        <div class="column col2">
            <?php echo do_shortcode('[contact-form-7 title="main contact form"]'); ?>
        </div>
    </div>
</section>
<script>
(function() {

    // updates file input - https://jsfiddle.net/chintanbanugaria/uzva5byy/
    var file = document.getElementById("file");
    file.onchange = function(){
        if(file.files.length > 0) {
        document.getElementById('filename').innerHTML = file.files[0].name;
        }
    };

    //let checkboxes = document.getElementsByClassName('wpcf7-list-item');
    // checkboxes.forEach(function(el) {
    //     el.classList.add("solet")
    // });

    // Get the button, and when the user clicks on it, execute myFunction
    //document.querySelector(".fa-chevron-circle-up").onclick = function() { toggleContact() };

    /* myFunction toggles between adding and removing the show class, which is used to hide and show the dropdown content */
    // function toggleContact() {
    //     document.getElementById("contact-wrap").classList.toggle("hidden");
    //     document.getElementById("contact-wrap").classList.toggle("show");
    // }
})();
</script>