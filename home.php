<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>

<style>
/*.picture-item {width:250px!important;} .picture-item img{display:block;width:100%}@supports ((-o-object-fit:cover) or (object-fit:cover)){.picture-item img{-o-object-fit:cover;height:100%;max-width:none;object-fit:cover}}.picture-item--h2{height:464px}.picture-item__inner{background:#ecf0f1;height:100%;overflow:hidden;position:relative}.picture-item__description{margin:0;padding:0 2em 1em 1em;width:100%}.picture-item__title{flex-shrink:0;margin-right:4px}@media screen and (min-width:768px){.picture-item--overlay .picture-item__details{background-color:rgba(0,0,0,.6);bottom:0;color:#fff;left:0;overflow:hidden;position:absolute;width:100%}.picture-item--overlay .picture-item__description{display:none}@supports ((-webkit-filter:blur(1px)) or (filter:blur(1px))) and ((-webkit-clip-path:inset(0 0 0 0)) or (clip-path:inset(0 0 0 0))){.picture-item--overlay .picture-item__blur{-webkit-clip-path:inset(170px 0 0 0);-webkit-filter:blur(7px);clip-path:inset(170px 0 0 0);display:block;filter:blur(7px);left:0;position:absolute;top:0;z-index:1}.picture-item--overlay .picture-item__details{background:none}.picture-item--overlay .picture-item__tags,.picture-item--overlay .picture-item__title{position:relative;z-index:2}}}.my-shuffle-container{overflow:hidden;position:relative}.sizer{opacity:0;position:absolute;visibility:hidden}.shuffle--animatein{overflow:visible}.shuffle--animatein .picture-item__inner{-webkit-transform:translateY(220px);opacity:0;transform:translateY(220px)}.shuffle--animatein .picture-item__inner--transition{transition:all .6s ease}.shuffle--animatein .picture-item.in .picture-item__inner{-webkit-transform:translate(0);opacity:1;transform:translate(0)}@media screen and (max-width:767px){.picture-item{height:auto;margin-top:20px}.picture-item__description,.picture-item__details{font-size:.875em;padding:.625em}.picture-item__description{padding-bottom:1.25em;padding-right:.875em}.picture-item--h2{height:auto}}*/
</style>

<div id="primary" class="content-area">

	<main id="main" class="site-main wrapper" role="main">

		<section class="tile pb0">
			<div class="container">

				<div class="tile is-ancestor">

				  <div class="tile is-3 is-parent">
					<a class="box-link" href="/catalogo">
						<div class="tile is-child box">
								<div class="box-wrapper first-banner-box">
									<small class="gradient">Ideas de producción EraGroup</small>
									<h2>Etiquetas textiles para indumentaria</h2>
									<small>Explorá materiales y diseños &rarr;</small>
								</div>
						</div>
					</a>
  				</div>

					<div class="tile is-3 is-parent">
					<?php
						$term   = get_term( 2, 'tipo_etiqueta' );
						//var_dump($term );
						$link   = get_term_link( $term->term_id, 'tipo_etiqueta');
						$desc   = get_field('grilla_texto', 'tipo_etiqueta_2');
						$bajada = get_field('bajada', 'tipo_etiqueta_2');
						$cta    = get_field('call_to_action', 'tipo_etiqueta_2');
						$img    = apply_filters( 'jetpack_photon_url', get_field('featured_image', 'tipo_etiqueta_2'));
						$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_2');
						$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_2');
						$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_2');
						$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_2');
						$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_2');
						$deg_rot= get_field('direccion_degrade', 'tipo_etiqueta_2');
						$clean_slug = str_replace('etiquetas-','',$term->slug);
					?>
							<a class="box-link" href="/catalogo?filter=<?php echo $clean_slug; ?>">
	  				    <div class="tile is-child box" style="background: linear-gradient(<?php echo $deg_rot.','.$deg_c1.','.$deg_c2 ?>);">

										<div class="box-wrapper gradient-bg bigger-box">
											<img class="featured_image" src="<?php echo $img ?>" style="position:relative; transform: rotate(<?php echo $img_rot ?>deg); top:<?php echo $img_ofy ?>px; right:<?php echo $img_ofx ?>px;">

											<div class="tile-box-content">
												<p class="title"><span>Etiquetas</span><br>  <?php echo $term->name ?></p>
											  <p class="bajada"><?php echo $bajada ?></p>
											  <p class="desc"><?php echo $desc ?></p>
											  <p class="cta"><?php echo $cta ?> &rarr; </p>
											</div>
										</div>

	  				    </div>
							</a>
  				  </div>
				  <div class="tile is-vertical is-parent">
					<?php
						$term   = get_term( 3, 'tipo_etiqueta' );
						//var_dump($term );
						$link   = get_term_link( $term->term_id, 'tipo_etiqueta');
						$desc   = get_field('grilla_texto', 'tipo_etiqueta_3');
						$bajada = get_field('bajada', 'tipo_etiqueta_3');
						$cta    = get_field('call_to_action', 'tipo_etiqueta_3');
						$img    = apply_filters( 'jetpack_photon_url', get_field('featured_image', 'tipo_etiqueta_3'));
						$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_3');
						$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_3');
						$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_3');
						$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_3');
						$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_3');
						$deg_rot= get_field('direccion_degrade', 'tipo_etiqueta_3');
						$clean_slug = str_replace('etiquetas-','',$term->slug);
					?>

						<div class="tile is-child box" style="background: linear-gradient(<?php echo $deg_rot.','.$deg_c1.','.$deg_c2 ?>);">

							<a class="box-link" href="/catalogo?filter=<?php echo $clean_slug ?>">
								<div class="box-wrapper gradient-bg small-box small-box-1">
									<img class="featured_image" src="<?php echo $img ?>" style="position:relative; transform: rotate(<?php echo $img_rot ?>deg); top:<?php echo $img_ofy ?>px; right:<?php echo $img_ofx ?>px;">

									<div class="tile-box-content">
										<div class="top-row">
											<p class="title"><span>Etiquetas</span><br>  <?php echo $term->name ?></p>
											<p class="bajada"><span class="bajada-title has-text-weight-semibold"><?php echo $bajada ?></span><br> <span class="mini-desc"><?php echo $desc ?></span></p>
											<p class="small-card-arrow">&rarr;</p>
										</div>
										<div class="cta-wrap">
											<small class="cta"><?php echo $cta ?> <span class="large-card-arrow">&rarr;</span> </small>
										</div>
									</div>
								</div>
							</a>

						</div>

						<?php
							$term   = get_term( 4, 'tipo_etiqueta' );
							//var_dump($term );
							$link   = get_term_link( $term->term_id, 'tipo_etiqueta');
							$desc   = get_field('grilla_texto', 'tipo_etiqueta_4');
							$bajada = get_field('bajada', 'tipo_etiqueta_4');
							$cta    = get_field('call_to_action', 'tipo_etiqueta_4');
							$img    = apply_filters( 'jetpack_photon_url', get_field('featured_image', 'tipo_etiqueta_4'));
							$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_4');
							$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_4');
							$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_4');
							$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_4');
							$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_4');
							$deg_rot= get_field('direccion_degrade', 'tipo_etiqueta_4');
							$clean_slug = str_replace('etiquetas-','',$term->slug);
						?>

					    <div class="tile is-child box" style="background: linear-gradient(<?php echo $deg_rot.','.$deg_c1.','.$deg_c2 ?>);">

							<a class="box-link" href="/catalogo?filter=<?php echo $clean_slug ?>">
								<div class="box-wrapper gradient-bg small-box small-box-2">
									<img class="featured_image" src="<?php echo $img ?>" style="position:relative; transform: rotate(<?php echo $img_rot ?>deg); top:<?php echo $img_ofy ?>px; right:<?php echo $img_ofx ?>px;">

									<div class="tile-box-content">
										<div class="top-row">
											<p class="title"><span>Etiquetas</span><br>  <?php echo $term->name ?></p>
											<p class="bajada"><span class="bajada-title has-text-weight-semibold"><?php echo $bajada ?></span><br> <span class="mini-desc"><?php echo $desc ?></span></p>
											<p class="small-card-arrow">&rarr;</p>
										</div>
										<div class="cta-wrap">
											<small class="cta"><?php echo $cta ?> <span class="large-card-arrow">&rarr;</span> </small>
										</div>
									</div>
								</div>
							</a>

						</div>

				  </div>
				  <div class="tile is-vertical is-parent">

				  <?php
						$term   = get_term( 5, 'tipo_etiqueta' );
						//var_dump($term );
						$link   = get_term_link( $term->term_id, 'tipo_etiqueta');
						$desc   = get_field('grilla_texto', 'tipo_etiqueta_5');
						$bajada = get_field('bajada', 'tipo_etiqueta_5');
						$cta    = get_field('call_to_action', 'tipo_etiqueta_5');
						$img    = apply_filters( 'jetpack_photon_url', get_field('featured_image', 'tipo_etiqueta_5'));
						$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_5');
						$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_5');
						$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_5');
						$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_5');
						$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_5');
						$deg_rot= get_field('direccion_degrade', 'tipo_etiqueta_5');
						$clean_slug = str_replace('etiquetas-','',$term->slug);
					?>

						<div class="tile is-child box" style="background: linear-gradient(<?php echo $deg_rot.','.$deg_c1.','.$deg_c2 ?>);">

							<a class="box-link" href="/catalogo?filter=<?php echo $clean_slug ?>">
								<div class="box-wrapper gradient-bg small-box small-box-3">
									<img class="featured_image" src="<?php echo $img ?>" style="position:relative; transform: rotate(<?php echo $img_rot ?>deg); top:<?php echo $img_ofy ?>px; right:<?php echo $img_ofx ?>px;">

									<div class="tile-box-content">
										<div class="top-row">
											<p class="title"><span>Etiquetas</span><br>  <?php echo $term->name ?></p>
											<p class="bajada"><span class="bajada-title has-text-weight-semibold"><?php echo $bajada ?></span><br> <span class="mini-desc"><?php echo $desc ?></span></p>
											<p class="small-card-arrow">&rarr;</p>
										</div>
										<div class="cta-wrap">
											<small class="cta"><?php echo $cta ?> <span class="large-card-arrow">&rarr;</span> </small>
										</div>
									</div>
								</div>
							</a>

						</div>

						<?php
						$term   = get_term( 6, 'tipo_etiqueta' );
						//var_dump($term );
						$link   = get_term_link( $term->term_id, 'tipo_etiqueta');
						$desc   = get_field('grilla_texto', 'tipo_etiqueta_6');
						$bajada = get_field('bajada', 'tipo_etiqueta_6');
						$cta    = get_field('call_to_action', 'tipo_etiqueta_6');
						$img    = apply_filters( 'jetpack_photon_url', get_field('featured_image', 'tipo_etiqueta_6'));
						$img_rot= get_field('rotacion_imagen', 'tipo_etiqueta_6');
						$img_ofx= get_field('image_offset_x', 'tipo_etiqueta_6');
						$img_ofy= get_field('image_offset_y', 'tipo_etiqueta_6');
						$deg_c1 = get_field('degrade_color_1', 'tipo_etiqueta_6');
						$deg_c2 = get_field('degrade_color_2', 'tipo_etiqueta_6');
						$deg_rot= get_field('direccion_degrade', 'tipo_etiqueta_6');
						$clean_slug = str_replace('etiquetas-','',$term->slug);
					?>

						<div class="tile is-child box" style="background: linear-gradient(<?php echo $deg_rot.','.$deg_c1.','.$deg_c2 ?>);">

							<a class="box-link" href="/catalogo?filter=<?php echo $clean_slug ?>">
								<div class="box-wrapper gradient-bg small-box small-box-4">
									<img class="featured_image" src="<?php echo $img ?>" style="position:relative; transform: rotate(<?php echo $img_rot ?>deg); top:<?php echo $img_ofy ?>px; right:<?php echo $img_ofx ?>px;">

									<div class="tile-box-content">
										<div class="top-row">
											<p class="title"><span>Etiquetas</span><br>  <?php echo $term->name ?></p>
											<p class="bajada"><span class="bajada-title has-text-weight-semibold"><?php echo $bajada ?></span><br> <span class="mini-desc"><?php echo $desc ?></span></p>
											<p class="small-card-arrow">&rarr;</p>
										</div>
										<div class="cta-wrap">
											<small class="cta"><?php echo $cta ?> <span class="large-card-arrow">&rarr;</span> </small>
										</div>
									</div>
								</div>
							</a>

						</div>
				  </div>

				</div>

			</div>
		</section>

		<section class="catalogo-grid mb50">

			<?php echo do_shortcode('[catalogo title="Inspirate con nuestro catálogo de etiquetas." tipo="" button_filter="yes" text_filter="no" order_filter="no" popups="yes" quantity="-1" random="true"]') ?>

			<div class="container">

				<div class="columns is-block has-text-centered pt70">

					<a class="button is-primary pl100 pr100" href="/catalogo">IR AL CATÁLOGO</a>

				</div>
			</div>


		</section>


		
		<section class="banner-social banner-whatsapp">
			<div class="container columns">
				<div class="column is-three-quarters first-col">
					<div class="pb40">
						<small class="has-text-weight-bold">Contactá con un asesor de manera rápida y ágil</small>
					</div>
					<div>
						<p class="bigger has-text-weight-semibold">Whatsappeanos y listo</p>
					</div>
					<div class="has-text-right pt40">
						<small class="has-text-weight-semibold"><img class="icon-wa" src="https://icon.now.sh/whatsapp" />Etiquetas para ropa al alcance de un Whatsapp</small>
					</div>
				</div>
				<div class="column has-text-right pr50 second-col">
					<a href="https://web.whatsapp.com/send?phone=5491132519188&text=Hola!,%20necesitar%C3%ADa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20etiquetas." class="button is-primary pl30 pr30">Enviar Whatsapp</a>
				</div>
			</div>
		</section>
		


		<section class="help-tile pb50">
			<div class="container">
				<div class="columns">
					<div class="column">
						<p class="section-title">Asimilar el rango de productos que podemos ofrecerte es clave para hacer etiquetas espectaculares a bajo costo.</p>
					</div>
					<!-- <div class="column is-one-quarter has-text-right">
						<a href="#">Conocer más recursos &rarr;</a>
					</div> -->
				</div>

				<div class="tile is-ancestor posts-first-row">
					<div class="tile">

				<?php
				
				// WP_Query posts
				$args = array(
					'posts_per_page' => '3',
				);

				// The Query
				$posts = new WP_Query( $args );

				// The Loop
				if ( $posts->have_posts() ) {
					while ( $posts->have_posts() ) {
						$posts->the_post(); ?>
						<div class="tile is-parent">
							<article class="tile is-child notification">
								<a href="#" class="blog-post-tile no-decoration">
									<p class="title"><?php echo get_the_title() ?></p>
									<div class="post-modal is-hidden">

											<article id="post-<?php get_the_ID(); ?>" <?php post_class('card'); ?>>
													<div class="card-image">
														<figure class="image popup-figure">
														<?php echo apply_filters( 'jetpack_photon_url', get_the_post_thumbnail(get_the_ID(), 'full', array( 'class' => 'nolazy' ))); ?>
														</figure>
													</div>
												<div class="card-content">
													<div class="">
														<p class="post-volanta"><?php echo get_field('volanta', get_the_ID()); ?></p>
														<header class="media-content">
															<h5><?php echo get_the_title() ?></h5>
														</header><!-- .entry-header -->
													</div>
													<div class="content entry-content">
													<?php echo apply_filters( 'the_content', get_the_content() ); ?>
													<div class="content entry-footer">
														<small><span class="consultanos" onclick="closeModalposts()">› Consultanos</span></small>
													</div>
												</div><!-- .entry-content -->
											</div>

											</article><!-- #post-## -->





									</div>
									</a>
							</article>
						</div>
					<?php
					}
				}

				// Restore original Post Data
				wp_reset_postdata();
				?>

					</div>
				</div>





				<div class="tile is-ancestor posts-second-row">
					<div class="tile">

				<?php
				
				// WP_Query posts
				$args = array(
					'posts_per_page' => 3,
					'offset' => 3
				);

				// The Query
				$posts = new WP_Query( $args );

				// The Loop
				if ( $posts->have_posts() ) {
					while ( $posts->have_posts() ) {
						$posts->the_post(); ?>
						<div class="tile is-parent">
							<article class="tile is-child notification">
								<a href="#" class="blog-post-tile no-decoration">
									<p class="title"><?php echo get_the_title() ?></p>
									<div class="post-modal is-hidden">

											<article id="post-<?php get_the_ID(); ?>" <?php post_class('card'); ?>>
													<div class="card-image">
														<figure class="image popup-figure">
															<?php echo apply_filters( 'jetpack_photon_url', get_the_post_thumbnail(get_the_ID(), 'full', array( 'class' => 'nolazy' ))); ?>
														</figure>
													</div>
												<div class="card-content">
													<div class="">
														<p class="post-volanta"><?php echo get_field('volanta', get_the_ID()); ?></p>
														<header class="media-content">
															<h5><?php echo get_the_title() ?></h5>
														</header><!-- .entry-header -->
													</div>
													<div class="content entry-content">
													<?php echo apply_filters( 'the_content', get_the_content() ); ?>
													<div class="content entry-footer">
														<small><span class="consultanos" onclick="closeModalposts()">› Consultanos</span></small>
													</div>
												</div><!-- .entry-content -->
											</div>

											</article><!-- #post-## -->





									</div>
									</a>
							</article>
						</div>
					<?php
					}
				}

				// Restore original Post Data
				wp_reset_postdata();
				?>

					</div>
				</div>



		</section>






		<section class="necesitas has-padding">
			<div class="container columns has-text-centered">
				<div class="column is-8 is-offset-2">
					<div class="direction-row pb20">
						<img width="100" src="https://i1.wp.com/eragroup.com.ar/wp-content/uploads/2019/07/iconos_eragroup.png" class="icons" alt="EraGroup icons" />
					</div>
					<h2 class="has-text-weight-semibold">¿Necesitás  etiquetas  para  ropa?</h2>
					<p>¡Contá con nosotros! Somos Eragroup Etiquetas y las etiquetas son nuestro día a día, si necesitás producir tu etiqueta de marca contactate con nosotros para cotizar lo que necesitás.</p>
				</div>
			</div>
			<div class="columns max-100 is-hidden-touch">
				<div class="column"></div>
				<div class="column">
				<a href="https://api.whatsapp.com/send?phone=5491132519188&text=Hola!,%20necesitar%C3%ADa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20etiquetas."><button class="button is-primary-era"><span>CONSULTAR A UN ASESOR</span></button></a>
				</div>
				<div class="column">
				<a href="/catalogo"><button class="button is-primary-era mr0"><span>VER CATÁLOGO</span></button></a>
				</div>
				<div class="column"></div>
			</div>

			<div class="columns max-100 is-hidden-desktop">
				<div class="column"></div>
				<div class="column">
				<a href="https://api.whatsapp.com/send?phone=5491132519188&text=Hola!,%20necesitar%C3%ADa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20etiquetas."><button class="button is-primary-era"><span>CONSULTAR A UN ASESOR</span></button></a>
				</div>
				<div class="column">
					<a href="/catalogo"><button class="button is-primary-era mr0"><span>VER CATÁLOGO</span></button></a>
				</div>
				<div class="column">
				<a href="tel:+541152541732"><button class="button is-primary-era"><span>LLAMAR POR TELÉFONO</span></button></a>
				</div>
				<div class="column"></div>
			</div>

		</section>

		<section class="banner-social banner-instagram">
			<div class="container columns">
				<div class="column is-three-quarters first-col">
					<div class="pb40">
						<small class="has-text-weight-bold">Facebook | Instagram</small>
					</div>
					<div>
						<p class="bigger has-text-weight-semibold">Seguinos en las redes</p>
					</div>
					<div class="pt40">
						<small class="has-text-weight-semibold">Diariamente publicamos contenido nuevo e interesante para que seguirnos sea importante.</small>
					</div>
				</div>
				<div class="column flex-align-center direction-row second-col">
					<a class="icon-link" href="https://www.facebook.com/eragroupetiquetas/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a class="icon-link" href="https://www.instagram.com/eragroupetiquetas/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				</div>
			</div>
		</section>

		<?php get_template_part( 'template-parts/contact', 'row' ); ?>


		<section class="columns home-bottom-boxes">
			<div class="column">
				<div class="turc-to-pur br6 has-text-white padding">
					<p>Envíos a todo el país</p>
					<p class="bigger">Entrega a domicilio</p>
					<small>Bonificación en CABA</small>
				</div>
			</div>
			<div class="column">
				<div class="pink-to-pur br6 has-text-white padding">
					<p>Puesta a punto de originales</p>
					<p class="bigger">Servicio de preimpresión</p>
					<small>Eliminá el margen de error</small>
				</div>
			</div>
			<div class="column is-half">
				<div class="turc-to-blue br6 has-text-white padding">
					<p>Descubrí nuevo materiales y usos</p>
					<p class="bigger">Mantenete actualizado</p>
					<small>Seguinos en Facebook e Instagram</small>
				</div>
			</div>
		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php //get_sidebar(); ?>
<script>
tippy('.picture-item', {
	html: el => el.querySelector('.popup'),
	interactive: true,
	flip: false,
	onShow: function() {
		this.querySelector('#brnPrepend').addEventListener("click", function(e){
			var sharableURL = this.getAttribute('data-etiqueta-link');
			
			// Get the modal
			var modal = document.getElementById('myModal');
			var modalBG = document.getElementsByClassName("modal-background")[0];
			var modalContent = document.getElementsByClassName("modal-content")[0];
			var span = document.getElementsByClassName("modal-close")[0];
			var shareContents = document.getElementsByClassName("share-contents")[0];

			// if( modal.querySelector('article') != null) {
			// 	modalContent.removeChild( modal.querySelector('article') );
			// }
			shareContents.classList.remove("is-hidden");
			
			modal.classList.add("is-active");
				
			span.onclick = function() {
				modal.classList.remove("is-active");
			}
			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modalBG) {
					modal.classList.remove("is-active");
				}
			}
			
			// edit modal content
			//shareContent.innerHTML = sharableURL;
			
			// for each que agarra todos los botones del popup, copie lo tengan en el href, y agregen sharableURL al final
			document.querySelectorAll('.is-active a.share-btn').forEach(function(elem, i) {
				//if(elem.href.indexOf("sharableURL") == -1 ) {
					elem.href = elem.getAttribute('data-sharer') + sharableURL;
				//}
			});
			
			e.preventDefault();
		});
		this.querySelector('#brnConsulta').addEventListener("click", function(e){
			e.preventDefault();
			var contactForm = document.getElementById('contacto');
			contactForm.scrollIntoView();
			// fill in contact
			contactForm.querySelector('.wpcf7-textarea').value = "Estoy interesado en etiquetas de tipo" + this.getAttribute('data-etiqueta-tipo');
		});
	}
});

document.querySelectorAll('.thumbnail').forEach(function(elem) {

		elem.onclick = function(e) {

				const src = elem.getAttribute('data-src')
	const src_full = elem.getAttribute('data-src-full')
				const html = '<a href="'+src_full+'" target="_blank"><img src="' + src + '" class="lightbox-image" /></a>'

				basicLightbox.create(html).show()

				document.querySelector(".basicLightbox__placeholder").onclick = function() {closeLightbox()};
				function closeLightbox() {
						document.querySelector('.basicLightbox').remove();
				}

		}

})
document.addEventListener("DOMContentLoaded", function(){
	// extends the filter viewport
	let figureHeight = document.querySelector("figure.shuffle-item:first-child").offsetHeight;
	document.querySelector("body.home .grid-filter.shuffle").style.maxHeight=figureHeight*2+"px";	
});

var classname = document.getElementsByClassName("blog-post-tile");

for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('click', function(e) {
			
			// Get the modal
			var modal = document.getElementById('myModal');
			var modalBG = document.getElementsByClassName("modal-background")[0];
			var span = document.getElementsByClassName("modal-close")[0];
			var modalContent = document.getElementsByClassName("modal-content")[0];
			var modalContentPosts = document.getElementsByClassName("post-contents")[0];
			var shareContents = document.getElementsByClassName("share-contents")[0];
			
			modal.classList.add("is-active");
			shareContents.classList.add("is-hidden");
				
			// span.onclick = function() {
			// 	modal.classList.remove("is-active");
			// 	//shareContents.classList.remove("is-hidden");
			// }
			
			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modalBG) {
					modal.classList.remove("is-active");
					shareContents.classList.remove("is-hidden");
					modalContentPosts.innerHTML = "";
				}
			}

			e.preventDefault();
			modalContentPosts.innerHTML = this.parentNode.querySelector('.post-modal').innerHTML;

		});
}

document.querySelector(".posts-first-row .tile.is-parent:first-child").classList.add('is-6');
document.querySelector(".posts-second-row .tile.is-parent:nth-child(2)").classList.add('is-6');

</script>
<?php get_footer(); ?>
